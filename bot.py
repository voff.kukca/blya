import shutil
from asyncio import sleep
import nacl
import discord
from pytube import YouTube
import os
import glob
from discord.ext import commands

intents = discord.Intents.all()
client = commands.Bot(command_prefix='.', intents=intents)

@client.command()
async def hi(ctx):
    """
    Sends a greeting message "idi nahui" to the channel and deletes the triggering message.

    Parameters
    ----------
    ctx : Context
        The context object passed by the Discord API, containing information about the message and the channel.

    Returns
    -------
    None
    """
    await ctx.send('idi nahui')
    await ctx.message.delete()

@client.command()
async def join(ctx):
    """
    The `join` command allows the bot to join the voice channel that the author of the command is in.
    If the author is not in a voice channel, a message will be sent to the channel indicating that the author is not in a channel.
    If the author is in a voice channel with a specific name "Разговорная𝗽𝟭𝘁𝘁", a message will be sent to the channel indicating that the author is banned.
    """
    destination = str(ctx.author.voice.channel) + str(ctx.author.name)
    if destination == 'Разговорная𝗽𝟭𝘁𝘁':
        await ctx.send('ы забаненен?')
        await ctx.message.delete()
    else:
        channel = ctx.author.voice.channel
        if channel != None:
            voice_channel = channel.name
            vc = await channel.connect()
            vc.play(discord.FFmpegPCMAudio(executable="ffmpeg.exe",
                                           source="muzika/nuzdiki.mp3"))
            while vc.is_playing():
                await sleep(.1)
            await vc.disconnect()
        else:
            await ctx.send(str(ctx.author.name) + "is not in a channel.")
        await ctx.message.delete()

@client.command()
async def play(ctx, arg):
    """
    This function is used to play a YouTube video in a Discord voice channel.

    Parameters:
        ctx (discord.ext.commands.Context): The context in which the command was invoked.
        arg (str): The YouTube video URL to be played.
    """

    # Set the destination directory for the downloaded video
    destination = str(ctx.author.voice.channel) + str(ctx.author.name)
    # Egor zabanen
    i=0
    if os.path.isdir(destination):
        for filename in os.listdir(destination):
            i+=1
    if destination == 'Разговорная𝗽𝟭𝘁𝘁' or i > 10:
        await ctx.send('ы забаненен?')
        print(len(glob.glob(str(destination))))
        await ctx.message.delete()
    else:
        # Retrieve the video from the YouTube API
        yt = YouTube(str(arg))
        video = yt.streams.filter(only_audio=True).first()
        # Attempt to retrieve the author's voice channel
        try:
            channel = ctx.author.voice.channel
        except discord.ext.commands.errors.CommandInvokeError:
            pass
        # If the video is less than 5000 seconds in length
        if int(yt.length)<5000:
            # Download the video
            out_file = video.download(output_path=destination)

            # Rename the downloaded files to sequential numbers
            i = 1
            for filename in os.listdir(destination):
                my_dest = str(i) + ".mp3"
                my_source = destination + '\\' + filename
                my_dest = destination + '\\' + my_dest
                try:
                    # Attempt to rename the file
                    try:
                        os.rename(my_source, my_dest)
                    except PermissionError:
                        pass
                except discord.ext.commands.errors.CommandInvokeError:
                    pass
                i += 1
            # Print a success message indicating that the video has been downloaded
            print(yt.title + " has been successfully downloaded.")
            # If the author is in a voice channel
            if channel != None:
                # Retrieve the voice channel name
                voice_channel = channel.name
                # Attempt to connect to the voice channel
                try:
                    vc = await channel.connect()
                except discord.ext.commands.errors.CommandInvokeError or discord.errors.ClientException:
                    pass
                # Play each of the renamed files in the voice channel
                i = 1
                for filename in os.listdir(destination):
                    vc.play(discord.FFmpegPCMAudio(executable="ffmpeg.exe",
                                                    source=str(destination + '\\' + filename)))
                    while vc.is_playing():
                        await sleep(.1)
                    os.remove(destination + '\\' + filename)
                # Disconnect from the voice channel
                await vc.disconnect()
            else:
                # If the author is not in a voice channel, send a message indicating this
                await ctx.send(str(ctx.author.name) + "is not in a channel.")
        else:
            # If the video is longer than 5000 seconds, send 'пошел нахуй ты ебанулся?'
            await ctx.send('пошел нахуй ты ебанулся?')
            channel = ctx.author.voice.channel
    # Delete message
    await ctx.message.delete()


@client.command()
async def add(ctx, arg):
    """
    Joins the voice channel of the author and adds a new song to the playlist from YouTube.

    The song is specified by the `arg` parameter, which is the URL of the YouTube video. If the length of the video
    is less than 50000 seconds, it is downloaded and added to the playlist. If the length of the video is greater
    than 50000 seconds, the command sends a message saying "блять нахуй не буду".

    The function also checks if the author is in the voice channel "Разговорная𝗽𝟭𝘁𝘁", and if so, sends a message "ы забаненен?" and deletes the message.
    """
    destination = str(ctx.author.voice.channel) + str(ctx.author.name)
    if destination == 'Разговорная𝗽𝟭𝘁𝘁':
        await ctx.send('ы забаненен?')
        await ctx.message.delete()
    else:
        yt = YouTube(str(arg))
        video = yt.streams.filter(only_audio=True).first()
        if int(yt.length) < 50000:
            destination = '.'
            out_file = video.download(output_path=destination)
            base, ext = os.path.splitext(out_file)
            new_file = base + '.mp3'
            os.rename(out_file, new_file)
            print(yt.title + " has been successfully downloaded.")
            playlist.append(yt.title)
        else:
            await ctx.send('блять нахуй не буду')
        await ctx.message.delete()

@client.command()
async def leave(ctx):
    """Disconnects the bot from the voice channel the author is in.

    :param ctx: the context of the command, which includes information about the message and its author
    :type ctx: discord.ext.commands.Context

    :raises: ValueError if the author is not in a voice channel
    """
    destination = str(ctx.author.voice.channel) + str(ctx.author.name)
    if destination == 'Разговорная𝗽𝟭𝘁𝘁':
        await ctx.send('ы забаненен?')
        await ctx.message.delete()
    else:
        if ctx.voice_client is None:
            raise ValueError("The bot is not currently in a voice channel.")
        await ctx.voice_client.disconnect()
        await ctx.message.delete()

@client.command()
async def clear(ctx):
    destination = str(ctx.author.voice.channel) + str(ctx.author.name)
    if destination == 'Разговорная𝗽𝟭𝘁𝘁':
        await ctx.send('ы забаненен?')
        await ctx.message.delete()
    else:
        await ctx.voice_client.disconnect()
        destination = str(ctx.author.voice.channel) + str(ctx.author.name)
        shutil.rmtree(destination)
        await ctx.message.delete()

@client.command()
async def egor(ctx):
    """Plays the 'Poshel na Huy Che Stosh' song in the author's voice channel."""
    channel = ctx.author.voice.channel
    if channel != None:
        voice_channel = channel.name
        vc = await channel.connect()
        vc.play(discord.FFmpegPCMAudio(executable="ffmpeg.exe",
                                       source="muzika/poshel-na-huy-che-stosh.mp3"))
        while vc.is_playing():
            await sleep(.1)
        await vc.disconnect()
    else:
        await ctx.send(str(ctx.author.name) + " is not in a voice channel.")
    await ctx.message.delete()

@client.command()
async def h(ctx):
    await ctx.send('egor: Command not specified.\n')

    await ctx.send("hi: Sends the message 'idi nahui' to the chat and deletes the invoked command message.\n")

    await ctx.send("clear: Command not specified.\n")

    await ctx.send("play: Plays a YouTube video in the author's voice channel. The video URL is passed as an argument. The video is downloaded to a folder with a name that consists of the voice channel name and the author's name. If the video length is less than 5000 seconds and the author is in a voice channel, the video is played in the voice channel.\n")

    await ctx.send("add: Command not specified.\n")

    await ctx.send("join: Attempts to connect the bot to the author's voice channel and play an audio file named 'muzika/nuzdiki.mp3'. Sends a message 'is not in a channel.' if the author is not in a voice channel. If the destination folder name is equal to 'Разговорная𝗽𝟭𝘁𝘁' or the number of files in the destination folder is greater than 10, sends the message 'ы забаненен?' and deletes the invoked command message.\n")

    await ctx.send("leave: Command not specified.\n")

    await ctx.send("help: Command not specified.'\n")


client.run('MTA3Mjg5NDQyNDcyMTQwODA4Mw.GDuD1Q.2iCjfsFuZ4sm4kZUK3Tmohx9vy3tk8Uzo6tuCU')